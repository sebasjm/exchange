/*
  This file is part of TALER
  Copyright (C) 2014-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file json/json_helper.c
 * @brief helper functions to generate specifications to parse
 *        Taler-specific JSON objects with libgnunetjson
 * @author Sree Harsha Totakura <sreeharsha@totakura.in>
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include "taler_util.h"
#include "taler_json_lib.h"


json_t *
TALER_JSON_from_amount (const struct TALER_Amount *amount)
{
  char *amount_str = TALER_amount_to_string (amount);

  GNUNET_assert (NULL != amount_str);
  {
    json_t *j = json_string (amount_str);

    GNUNET_free (amount_str);
    return j;
  }
}


json_t *
TALER_JSON_from_amount_nbo (const struct TALER_AmountNBO *amount)
{
  struct TALER_Amount a;

  TALER_amount_ntoh (&a,
                     amount);
  return TALER_JSON_from_amount (&a);
}


/**
 * Parse given JSON object to Amount
 *
 * @param cls closure, expected currency, or NULL
 * @param root the json object representing data
 * @param[out] spec where to write the data
 * @return #GNUNET_OK upon successful parsing; #GNUNET_SYSERR upon error
 */
static enum GNUNET_GenericReturnValue
parse_amount (void *cls,
              json_t *root,
              struct GNUNET_JSON_Specification *spec)
{
  const char *currency = cls;
  struct TALER_Amount *r_amount = spec->ptr;

  (void) cls;
  if (! json_is_string (root))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  if (GNUNET_OK !=
      TALER_string_to_amount (json_string_value (root),
                              r_amount))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  if ( (NULL != currency) &&
       (0 !=
        strcasecmp (currency,
                    r_amount->currency)) )
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  return GNUNET_OK;
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_amount (const char *name,
                        const char *currency,
                        struct TALER_Amount *r_amount)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_amount,
    .cleaner = NULL,
    .cls = (void *) currency,
    .field = name,
    .ptr = r_amount,
    .ptr_size = 0,
    .size_ptr = NULL
  };

  GNUNET_assert (NULL != currency);
  return ret;
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_amount_any (const char *name,
                            struct TALER_Amount *r_amount)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_amount,
    .cleaner = NULL,
    .cls = NULL,
    .field = name,
    .ptr = r_amount,
    .ptr_size = 0,
    .size_ptr = NULL
  };

  return ret;
}


/**
 * Parse given JSON object to Amount in NBO.
 *
 * @param cls closure, NULL
 * @param root the json object representing data
 * @param[out] spec where to write the data
 * @return #GNUNET_OK upon successful parsing; #GNUNET_SYSERR upon error
 */
static enum GNUNET_GenericReturnValue
parse_amount_nbo (void *cls,
                  json_t *root,
                  struct GNUNET_JSON_Specification *spec)
{
  const char *currency = cls;
  struct TALER_AmountNBO *r_amount = spec->ptr;
  const char *sv;

  (void) cls;
  if (! json_is_string (root))
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  sv = json_string_value (root);
  if (GNUNET_OK !=
      TALER_string_to_amount_nbo (sv,
                                  r_amount))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "`%s' is not a valid amount\n",
                sv);
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  if ( (NULL != currency) &&
       (0 !=
        strcasecmp (currency,
                    r_amount->currency)) )
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  return GNUNET_OK;
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_amount_nbo (const char *name,
                            const char *currency,
                            struct TALER_AmountNBO *r_amount)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_amount_nbo,
    .cleaner = NULL,
    .cls = (void *) currency,
    .field = name,
    .ptr = r_amount,
    .ptr_size = 0,
    .size_ptr = NULL
  };

  GNUNET_assert (NULL != currency);
  return ret;
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_amount_any_nbo (const char *name,
                                struct TALER_AmountNBO *r_amount)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_amount_nbo,
    .cleaner = NULL,
    .cls = NULL,
    .field = name,
    .ptr = r_amount,
    .ptr_size = 0,
    .size_ptr = NULL
  };

  return ret;
}


/**
 * Parse given JSON object to denomination public key.
 *
 * @param cls closure, NULL
 * @param root the json object representing data
 * @param[out] spec where to write the data
 * @return #GNUNET_OK upon successful parsing; #GNUNET_SYSERR upon error
 */
static enum GNUNET_GenericReturnValue
parse_denom_pub (void *cls,
                 json_t *root,
                 struct GNUNET_JSON_Specification *spec)
{
  struct TALER_DenominationPublicKey *denom_pub = spec->ptr;
  uint32_t cipher;
  struct GNUNET_JSON_Specification dspec[] = {
    GNUNET_JSON_spec_uint32 ("cipher",
                             &cipher),
    GNUNET_JSON_spec_uint32 ("age_mask",
                             &denom_pub->age_mask.mask),
    GNUNET_JSON_spec_end ()
  };
  const char *emsg;
  unsigned int eline;

  (void) cls;
  if (GNUNET_OK !=
      GNUNET_JSON_parse (root,
                         dspec,
                         &emsg,
                         &eline))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  denom_pub->cipher = (enum TALER_DenominationCipher) cipher;
  switch (denom_pub->cipher)
  {
  case TALER_DENOMINATION_RSA:
    {
      struct GNUNET_JSON_Specification ispec[] = {
        GNUNET_JSON_spec_rsa_public_key (
          "rsa_public_key",
          &denom_pub->details.rsa_public_key),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (root,
                             ispec,
                             &emsg,
                             &eline))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
      return GNUNET_OK;
    }
  default:
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
}


/**
 * Cleanup data left from parsing denomination public key.
 *
 * @param cls closure, NULL
 * @param[out] spec where to free the data
 */
static void
clean_denom_pub (void *cls,
                 struct GNUNET_JSON_Specification *spec)
{
  struct TALER_DenominationPublicKey *denom_pub = spec->ptr;

  (void) cls;
  TALER_denom_pub_free (denom_pub);
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_denom_pub (const char *field,
                           struct TALER_DenominationPublicKey *pk)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_denom_pub,
    .cleaner = &clean_denom_pub,
    .field = field,
    .ptr = pk
  };

  return ret;
}


/**
 * Parse given JSON object to denomination signature.
 *
 * @param cls closure, NULL
 * @param root the json object representing data
 * @param[out] spec where to write the data
 * @return #GNUNET_OK upon successful parsing; #GNUNET_SYSERR upon error
 */
static enum GNUNET_GenericReturnValue
parse_denom_sig (void *cls,
                 json_t *root,
                 struct GNUNET_JSON_Specification *spec)
{
  struct TALER_DenominationSignature *denom_sig = spec->ptr;
  uint32_t cipher;
  struct GNUNET_JSON_Specification dspec[] = {
    GNUNET_JSON_spec_uint32 ("cipher",
                             &cipher),
    GNUNET_JSON_spec_end ()
  };
  const char *emsg;
  unsigned int eline;

  (void) cls;
  if (GNUNET_OK !=
      GNUNET_JSON_parse (root,
                         dspec,
                         &emsg,
                         &eline))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  denom_sig->cipher = (enum TALER_DenominationCipher) cipher;
  switch (denom_sig->cipher)
  {
  case TALER_DENOMINATION_RSA:
    {
      struct GNUNET_JSON_Specification ispec[] = {
        GNUNET_JSON_spec_rsa_signature (
          "rsa_signature",
          &denom_sig->details.rsa_signature),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (root,
                             ispec,
                             &emsg,
                             &eline))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
      return GNUNET_OK;
    }
  default:
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
}


/**
 * Cleanup data left from parsing denomination public key.
 *
 * @param cls closure, NULL
 * @param[out] spec where to free the data
 */
static void
clean_denom_sig (void *cls,
                 struct GNUNET_JSON_Specification *spec)
{
  struct TALER_DenominationSignature *denom_sig = spec->ptr;

  (void) cls;
  TALER_denom_sig_free (denom_sig);
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_denom_sig (const char *field,
                           struct TALER_DenominationSignature *sig)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_denom_sig,
    .cleaner = &clean_denom_sig,
    .field = field,
    .ptr = sig
  };

  return ret;
}


/**
 * Parse given JSON object to blinded denomination signature.
 *
 * @param cls closure, NULL
 * @param root the json object representing data
 * @param[out] spec where to write the data
 * @return #GNUNET_OK upon successful parsing; #GNUNET_SYSERR upon error
 */
static enum GNUNET_GenericReturnValue
parse_blinded_denom_sig (void *cls,
                         json_t *root,
                         struct GNUNET_JSON_Specification *spec)
{
  struct TALER_BlindedDenominationSignature *denom_sig = spec->ptr;
  uint32_t cipher;
  struct GNUNET_JSON_Specification dspec[] = {
    GNUNET_JSON_spec_uint32 ("cipher",
                             &cipher),
    GNUNET_JSON_spec_end ()
  };
  const char *emsg;
  unsigned int eline;

  (void) cls;
  if (GNUNET_OK !=
      GNUNET_JSON_parse (root,
                         dspec,
                         &emsg,
                         &eline))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  denom_sig->cipher = (enum TALER_DenominationCipher) cipher;
  switch (denom_sig->cipher)
  {
  case TALER_DENOMINATION_RSA:
    {
      struct GNUNET_JSON_Specification ispec[] = {
        GNUNET_JSON_spec_rsa_signature (
          "blinded_rsa_signature",
          &denom_sig->details.blinded_rsa_signature),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (root,
                             ispec,
                             &emsg,
                             &eline))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
      return GNUNET_OK;
    }
  default:
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
}


/**
 * Cleanup data left from parsing denomination public key.
 *
 * @param cls closure, NULL
 * @param[out] spec where to free the data
 */
static void
clean_blinded_denom_sig (void *cls,
                         struct GNUNET_JSON_Specification *spec)
{
  struct TALER_BlindedDenominationSignature *denom_sig = spec->ptr;

  (void) cls;
  TALER_blinded_denom_sig_free (denom_sig);
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_blinded_denom_sig (
  const char *field,
  struct TALER_BlindedDenominationSignature *sig)
{
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_blinded_denom_sig,
    .cleaner = &clean_blinded_denom_sig,
    .field = field,
    .ptr = sig
  };

  return ret;
}


/**
 * Closure for #parse_i18n_string.
 */
struct I18nContext
{
  /**
   * Language pattern to match.
   */
  char *lp;

  /**
   * Name of the field to match.
   */
  const char *field;
};


/**
 * Parse given JSON object to internationalized string.
 *
 * @param cls closure, our `struct I18nContext *`
 * @param root the json object representing data
 * @param[out] spec where to write the data
 * @return #GNUNET_OK upon successful parsing; #GNUNET_SYSERR upon error
 */
static enum GNUNET_GenericReturnValue
parse_i18n_string (void *cls,
                   json_t *root,
                   struct GNUNET_JSON_Specification *spec)
{
  struct I18nContext *ctx = cls;
  json_t *i18n;
  json_t *val;

  {
    char *i18nf;

    GNUNET_asprintf (&i18nf,
                     "%s_i18n",
                     ctx->field);
    i18n = json_object_get (root,
                            i18nf);
    GNUNET_free (i18nf);
  }

  val = json_object_get (root,
                         ctx->field);
  if ( (NULL != i18n) &&
       (NULL != ctx->lp) )
  {
    double best = 0.0;
    json_t *pos;
    const char *lang;

    json_object_foreach (i18n, lang, pos)
    {
      double score;

      score = TALER_language_matches (ctx->lp,
                                      lang);
      if (score > best)
      {
        best = score;
        val = pos;
      }
    }
  }

  {
    const char *str;

    str = json_string_value (val);
    if (NULL == str)
    {
      GNUNET_break_op (0);
      return GNUNET_SYSERR;
    }
    *(const char **) spec->ptr = str;
  }
  return GNUNET_OK;
}


/**
 * Function called to clean up data from earlier parsing.
 *
 * @param cls closure
 * @param spec our specification entry with data to clean.
 */
static void
i18n_cleaner (void *cls,
              struct GNUNET_JSON_Specification *spec)
{
  struct I18nContext *ctx = cls;

  (void) spec;
  GNUNET_free (ctx->lp);
  GNUNET_free (ctx);
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_i18n_string (const char *name,
                             const char *language_pattern,
                             const char **strptr)
{
  struct I18nContext *ctx = GNUNET_new (struct I18nContext);
  struct GNUNET_JSON_Specification ret = {
    .parser = &parse_i18n_string,
    .cleaner = &i18n_cleaner,
    .cls = ctx,
    .field = NULL, /* we want the main object */
    .ptr = strptr,
    .ptr_size = 0,
    .size_ptr = NULL
  };

  ctx->lp = (NULL != language_pattern) ? GNUNET_strdup (language_pattern) :
            NULL;
  ctx->field = name;
  *strptr = NULL;
  return ret;
}


struct GNUNET_JSON_Specification
TALER_JSON_spec_i18n_str (const char *name,
                          const char **strptr)
{
  const char *lang = getenv ("LANG");
  char *dot;
  char *l;
  struct GNUNET_JSON_Specification ret;

  if (NULL != lang)
  {
    dot = strchr (lang,
                  '.');
    if (NULL == dot)
      l = GNUNET_strdup (lang);
    else
      l = GNUNET_strndup (lang,
                          dot - lang);
  }
  else
  {
    l = NULL;
  }
  ret = TALER_JSON_spec_i18n_string (name,
                                     l,
                                     strptr);
  GNUNET_free (l);
  return ret;
}


/* end of json/json_helper.c */
